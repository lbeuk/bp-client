# Blueprint Client (bp-client)
Linux desktop configuration client. Meant to integrate with Blueprint Server
(https://gitlab.com/lbeuk/bp-srv).

## Meta-about
* **Author:** Luke Beukelman (https://www.gitlab.com/lbeuk)
* **Version:** 0.1.0 - Still in initial development
* **Language:** Rust

## About
This is a project in early development. The goal of Blueprint is to create a reliable, 
comprehensive, and entirely integrated mobile device management (MDM) for desktops within
the GNU/Linux ecosystem, as it is an area of extreme lacking that may prohibit large
organizations from using Linux where it is useful.

## Goals (in descending order of significance)
* Providing a GOOD end product, that has a minimal learning curve for easy and willingful
adoption in the enterprise market.
* Increase the open-source and Linux share of the software and desktop market by taking a
trickle down approach, focusing on the enterprise desktop share in order to make impacts
in bulk.
    * Related, increasing development in Linux ecosystems by increasing market share in
    critical areas that have large amounts of investment.
* Reduce comupter waste by making Linux a viable option for older hardware in businesses
that have large amounts of outdated computers on their way out.
* Provide non-enterprise users tools that mirror those of parental controls.

## Modules necessary to achieve goal
Note: All mentions of control refer to centralized control through a configuration server.
* Dconf and configuration file control, with a friendly and modular front end to make common
tasks (such as setting pre-configured wallpapers) a trvial matter.
* Software management, ideally through a flatpak-esque service where users can install
**pre-approved** software to their user account, as well as run provide scripts (say, for
adding printers, servers, downloading files, etc..)
    * This should ideally run as a wrapper to an or multiple existing solution(s), to
    avoid the development curve and immediately provide businesses with a list of
    functioning software.
    * This also needs to impose restrictions on means of installing software through
    non-approved routes, such as adding flatpak remotes to the user remote list.