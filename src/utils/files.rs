use std::path::PathBuf;
use std::fs::{OpenOptions, File};

pub fn get_abs_path(path: String) -> Option<String> {
    return Some(PathBuf::from(&path)
        .canonicalize().unwrap()
        .into_os_string().into_string().unwrap());
}

pub fn path_to_abs(path: &PathBuf) -> Option<String> {
    return Some(path.canonicalize().unwrap()
        .into_os_string().into_string().unwrap());
}

pub fn open_file(path: &PathBuf, do_read: bool, do_write: bool, create_if_ne: bool) -> File {
    let file = OpenOptions::new()
        .read(do_read)
        .write(do_write)
        .create(create_if_ne)
        .open(path)
        .unwrap();
    return file;
}