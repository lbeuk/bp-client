extern crate reqwest;

// JValue and TValue are Value for json and toml, respectively
use toml::Value as TValue;
use serde_json::Value as JValue;
use std::fs;

use crate::utils::files::get_abs_path;

const CONF_PATH: &str = "./blueprint.toml";

// This function is designed specifically for reading the blueprint.toml configuration
pub fn read_conf(module: &str) -> TValue {
    let conf_file = fs::read_to_string(
        get_abs_path(String::from(CONF_PATH))
            .expect("Invalid configuration path")
    )
        .expect("Something went wrong reading the file");

    let data: TValue = toml::from_str(&conf_file)
        .expect("Can not parse config toml file, please check the syntax");

    return data[module].clone();
}

pub fn json_from_string<S>(input: S) -> JValue where S: Into<String> {
    let converted = input.into();
    let json = serde_json::from_str(&converted[..]).unwrap();
    return json;
}