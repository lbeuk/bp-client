pub mod errors;
pub mod args;

pub fn verify_root() {
    let uid = users::get_current_uid();
    if uid != 0 {
        panic!(errors::NO_ROOT);
    }
}
