// System errors

pub const NO_ROOT: &str = "This command requires root access. Please try again with root access.";


// Parsing errors


pub fn CONF_FORMAT<A>(module: A) -> String where A: Into<String> {
    return format!("Error in reading {} module configuration in blueprint.toml.", module.into().as_str());
}


pub fn JSON_FORMAT<A>(module: A) -> String where A: Into<String> {
    return format!("Error in formatting of JSON sent from remote server for {} module", module.into().as_str())
}

// Terminal input errors

pub const ARG_AFTER_FLAG: &str = "Arguments can only be passed after flags";

pub fn UNKNOWN_FLAG<A>(flag: A) -> String where A: Into<String> {
    return format!("Unknown flag: -{}", flag.into().as_str());
}

pub fn NUM_ARGS<A>(cmd: A, num: usize) -> String where A: Into<String> {
    return  format!("{} only takes {} arguments", cmd.into().as_str(), num);
}

