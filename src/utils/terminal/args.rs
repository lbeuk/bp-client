use crate::utils::terminal::errors;

// Command enumeration is for handling commands more efficiently.
// Rather than having to compare arguments twice (once for verification, once for execution),
// verified arguments can be stored in a Command and then passed into a more efficient match statement later.
enum Command {
    Install(Vec<String>),
    Bind(Vec<String>),
    Help(Vec<String>)
}

// Takes input arguments, acts upon them using the argument handlers defined below
pub fn parse_args(args: &[String]) {
    let mut command_list: Vec<Command> = Vec::new();

    // Trims off first argument (command title) and gets length
    let args = &args[1..];
    let len = args.len();

    // Iterates over each argument
    for i in 0..len{
        let arg = &args[i];

        // If the argument is a flag, add each following argument as a flag argument until next flag
        if &arg[0..1] == "-" {
            let flag = String::from(&arg[1..]);
            let mut flag_args: Vec<String> = Vec::new();

            // Iterate over each following
            'nested: for j in i+1..len {
                let new_arg = &args[j];

                // If is not a flag, add to list of flag arguments
                if &new_arg[0..1] != "-" {
                    flag_args.push(new_arg.clone());
                }
                else {
                    break 'nested
                }
            }

            // Pushes arguments to list of commands
            let flag_str = &flag[..];
            command_list.push(
                match flag_str {
                    "i" | "install" => Command::Install(flag_args.clone()),
                    "b" | "bind" => Command::Bind(flag_args.clone()),
                    "h" | "help" => Command::Help(flag_args.clone()),
                    _ => {
                        // Exits and errors if any unknown flags are passed
                        panic!(errors::UNKNOWN_FLAG(flag_str));
                    }
                }
            );
        }
        else if command_list.len() == 0 {
            panic!(errors::ARG_AFTER_FLAG);
        }
    }

    // Runs commands in command list
    for cmd in &command_list {
        match cmd {
            Command::Install(value) => handle_install(value),
            Command::Bind(value) => handle_bind(value),
            Command::Help(value) => handle_help(value)
        }
    }
}

// CLI argument handlers

fn handle_install(args: &Vec<String>) {
    if args.len() != 0 {
        panic!(errors::NUM_ARGS("Install", 0));
    }
    else
    {
        //verify_root();
        println!("Running install...");
        crate::utils::installer::install_all_modules();
    }
}

fn handle_bind(args: &Vec<String>) {
    if args.len() != 0 {
        panic!(errors::NUM_ARGS("Bind", 0));
    } else {
        //verify_root();
        println!("Running bind...");
        crate::control::bind::bind_to_bp_server();
    }
}

fn handle_help(args: &Vec<String>) {
    if args.len() != 0 {
        panic!(errors::NUM_ARGS("Help", 0));
    } else {
        println!("Blueprint client - Help");
        println!("----------------------------------------------");
        println!("This program is still under intense development. Assume no features are");
        println!("either complete or permanent. To track the project, go to https://gitlab.com/lbeuk/bp-client.");
        println!();
        println!("-i, -install                  Installs all modules");
        println!("-b, -bind                     Bind to configured server");
        println!("-h. -help                     Print CLI help page");
    }
}