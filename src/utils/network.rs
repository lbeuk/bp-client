use toml::Value as TValue;
use serde_json::Value as JValue;
use crate::utils::parser::read_conf;
use std::convert::TryInto;

pub fn get_server_url() -> String {
    let data: TValue = read_conf("server");

    let url = format!("{}://{}:{}",
        data["protocol"].as_str()
            .expect("Server config protocol is not a string."),
        data["address"].as_str()
            .expect("Server config address is not a string."),
        data["port"].as_str()
            .expect("Server config port is not a string."));
    return url;
}

pub fn server_rpc_call(rpc_json: JValue) -> JValue{
    let url = get_server_url();
    let url = format!("{}/rpc/", url);

    return reqwest::blocking::Client::new().post(&url)
        .json(&rpc_json).send()
        .unwrap().json().unwrap();
}