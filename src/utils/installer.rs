use crate::config::{dconf};

pub fn install_all_modules() {
    dconf::install();
}