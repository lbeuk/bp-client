use crate::utils::network::server_rpc_call;
use serde_json::Value as JValue;

// Creates an RPC request for binding and executes
pub fn request_binding() {

    let request = JValue::Null;

    server_rpc_call(request);

}



pub fn bind_to_bp_server() {
    request_binding();
}