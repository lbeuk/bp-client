use std::process::Command;
use std::fs::create_dir_all;
use std::path::PathBuf;
use std::io::Write;
use toml::Value as TValue;
use bp_conventions::conventions;

use crate::utils::parser::read_conf;
use crate::utils::files::open_file;
use crate::utils::network;
use crate::utils::terminal::errors;

const DCONF_DIR: &str = "/etc/dconf/";

// Step 1: Create directories for database
fn create_db_dirs(conf_data: &TValue) -> PathBuf{
    // Pulls data from blueprint.toml conf file
    let base_dir = conf_data["directory"].as_str();
    let db_name = conf_data["profile"].as_str();

    match (base_dir, db_name) {
        // Unwraps the base directory for dconf and the name of the profile
        (Some(directory), Some(name)) => {

            // Converts base directory into path object
            let mut path = PathBuf::from(directory);

            // Verifies that config directory exists
            path.canonicalize().expect("Dconf config directory does not exist.");

            // Adds /db/{db_name}/locks/ to path and creates id DNE
            path.push("db");
            path.push(format!("{}.d", name));

            // Clones base directory for database (where keyfiles are held) for return
            let output_dir = path.clone();

            // Adds locks path to path to be created and creates
            path.push("locks");
            create_dir_all(&path);

            // Print status
            println!("* Created directories for dconf");

            // Returns the base directory of database
            return output_dir;
        },
        // If either unwraps fail, exit with message
        _ => panic!(errors::CONF_FORMAT("dconf"))
    }
}

// Step 2: Create profile
fn create_profile(conf_data: &TValue) {
    // Pulls data from blueprint.toml conf file
    let base_dir = conf_data["directory"].as_str();
    let profile_name = conf_data["profile"].as_str();
    let usr_dbs = conf_data["usr-dbs"].as_array();
    let sys_dbs = conf_data["sys-dbs"].as_array();

    match (base_dir, profile_name, usr_dbs, sys_dbs) {
        // Unwraps all data from cofiguration file
        (Some(directory), Some(name), Some(user_dbs), Some(system_dbs)) => {
            // Creates the path for the profile file, which is named according to blueprint.toml conf
            let mut path = PathBuf::from(&directory);
            path.push("profile");
            path.push(&name);

            // Creates output string to write output into
            let mut outstring = String::new();

            // Adds databases to file
            for db in user_dbs {
                outstring = format!("{}{}\n", outstring,
                                    format!("user-db:{}", &db));
            }
            for db in system_dbs {
                outstring = format!("{}{}\n", outstring,
                                    format!("system-db:{}", &db));
            }

            // Opens profile file and writes
            let mut file = open_file(&path, false, true, true);
            file.write_all(&outstring.as_bytes());

            // Print status
            println!("* Created dconf profile");
        },
        // If any unwraps fail, exit with message
        _ => panic!(errors::CONF_FORMAT("dconf"))
    }
}

// Step 3: Create keyfile
fn create_keyfile(db_path: &PathBuf) {
    // Pulls JSON from server
    let json_data = network::server_rpc_call(conventions::rpc::Request::new("get_assets", serde_json::json!(
    {
        "group_id": 0,
        "modules": ["dconf"]
    }
    )).dump());
    let rpc_res: conventions::rpc::Response = serde_json::from_value(json_data).expect(&errors::JSON_FORMAT("dconf"));
    let rpc_res = rpc_res.result["dconf"].as_object().expect(&errors::JSON_FORMAT("dconf"));

    // Creates the output string to be written to file, as well as an array for locks
    let mut outstring = String::new();
    let mut lock_paths = Vec::new();

    // Runs over every keygroup in sent JSON data
    for keygroup in rpc_res {
        // Pulls keygroup data from JSON
        let keygroup_uri = keygroup.0;
        let keys = keygroup.1.as_array().expect(&errors::JSON_FORMAT("dconf"));

        // Adds URI section to output string
        outstring = format!("{}{}\n", outstring,
                            format!("[{}]", &keygroup_uri));

        for keypair in keys {
            // Pulls key data from JSON
            let key = &keypair["key"].as_str().expect(&errors::JSON_FORMAT("dconf"));
            let value_set = &keypair["value"].as_object()   .expect(&errors::JSON_FORMAT("dconf"));

            let value = value_set.get("value")                      .expect(&errors::JSON_FORMAT("dconf"))
                .as_str()                                                     .expect(&errors::JSON_FORMAT("dconf"));
            let type_indicator = value_set.get("value_type")        .expect(&errors::JSON_FORMAT("dconf"))
                .as_str()                                                     .expect(&errors::JSON_FORMAT("dconf"));
            let lock = value_set.get("lock")                        .expect(&errors::JSON_FORMAT("dconf"))
                .as_bool()                                                    .expect(&errors::JSON_FORMAT("dconf"));

            // Creates output string for this line
            let line: String;

            // If the value is a string, it needs to be wrapped in single quotation marks for dconf
            if type_indicator == "String" || type_indicator == "URL" {
                line = format!("{}='{}'", &key, &value);
            } else {
                line = format!("{}={}", &key, &value);
            }

            // Adds current line to outstring
            outstring = format!("{}{}\n", outstring, line);

            // Add lock path to lock_paths if key needs to be locked down
            if lock {
                let lock_path = format!("/{}/{}", &keygroup_uri, &key);
                lock_paths.push(lock_path);
            }
        }
        // Adds blank seperator line after every keygroup
        outstring = format!("{}\n", outstring);
    }

    // Get name of keyfile
    let keyfile_dconf = read_conf("dconf");
    let keyfile_name = keyfile_dconf["dbname"].as_str().expect(&errors::CONF_FORMAT("dconf"));

    // Defines path of keyfile
    let mut path = db_path.clone();
    path.push(&keyfile_name);

    // Write to keyfile
    let mut file = open_file(&path, false, true, true);
    file.write_all(
        &outstring.as_bytes()
    );

    // Print status
    println!("* Created dconf keyfile");


    // Create the lockfile from the given locks
    create_lockfile(&db_path, &keyfile_name, lock_paths);
}

// Step 4: Create lockfile
fn create_lockfile(db_path: &PathBuf, file_name: &str, lock_paths: Vec<String>) {
    // Creates file path in locks directory
    let mut path = db_path.clone();
    path.push("locks");
    path.push(&file_name);

    // Creates output string to write output into
    let mut outstring = String::new();

    // Creates
    for lock in lock_paths {
        outstring = format!("{}{}\n", outstring,
                            format!("{}", &lock));
    }

    let mut file = open_file(&path, false, true, true);
    file.write_all(outstring.as_bytes());

    // Print status
    println!("* Created dconf lockfile");
}

// Step 5: Update dconf databases
fn update_dconf() {
    Command::new("dconf")
        .arg("update")
        .output()
        .expect("Failed to execute, are you running as root?");

    // Print status
    println!("* Created dconf database");
}

// Install calls all steps to creating a dconf db from conf
pub fn install() {
    // Print status
    println!("Installing dconf module config");

    // Module data is pulled once before hand as it is reused
    let module_data = read_conf("dconf");

    // Creates directories, than profile, than keyfile, which calls upon creaing locks
    let db_path = create_db_dirs(&module_data);
    create_profile(&module_data);
    create_keyfile(&db_path);

    // Updates database
    update_dconf();
}