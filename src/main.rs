// Define module tree

use std::env;

use utils::terminal::args::parse_args;

// The control module defines functionality for interacting with configuration setters
mod control {
    pub mod bind;
}
// The config module defines functionality for setting configurations and system operations
mod config {
    pub mod dconf;
}
// The utils module defines tools common to other modules
mod utils {
    pub mod files;
    pub mod installer;
    pub mod network;
    pub mod parser;
    pub mod terminal;
}

fn main() {
    let args: Vec<String>  = env::args().collect();
    parse_args(&args);
}